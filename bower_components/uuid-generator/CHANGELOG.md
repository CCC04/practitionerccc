<a name="2.0.0"></a>
# [2.0.0](https://github.com/advanced-rest-client/uuid-generator/compare/1.0.4...2.0.0) (2018-03-30)


### Update

* Added new Travis configuration ([5a4c1dc0425be613380ce5f152d05b5b37820594](https://github.com/advanced-rest-client/uuid-generator/commit/5a4c1dc0425be613380ce5f152d05b5b37820594))



<a name="1.0.4"></a>
## [1.0.4](https://github.com/advanced-rest-client/uuid-generator/compare/1.0.3...v1.0.4) (2016-10-11)


### New

* Added dependencyci integration ([5337664ab164d85aded455375725523b6b6319a3](https://github.com/advanced-rest-client/uuid-generator/commit/5337664ab164d85aded455375725523b6b6319a3))



<a name="1.0.3"></a>
## [1.0.3](https://github.com/advanced-rest-client/uuid-generator/compare/1.0.2...v1.0.3) (2016-09-29)




<a name="1.0.2"></a>
## [1.0.2](https://github.com/advanced-rest-client/uuid-generator/compare/1.0.1...v1.0.2) (2016-09-21)


### Fix

* Fixed repository URL ([22718e1576fc10242c25ced8ef6041c3e27c4f7f](https://github.com/advanced-rest-client/uuid-generator/commit/22718e1576fc10242c25ced8ef6041c3e27c4f7f))



<a name="1.0.1"></a>
## 1.0.1 (2016-09-21)


### Fix

* Removed event variable from demo page ([3aa426bae7f7213818eef58b87d13701e0ac039f](https://github.com/--arc/uuid-generator/commit/3aa426bae7f7213818eef58b87d13701e0ac039f))

### New

* First release commit ([fb856f6486a1e76e8e3188be92593130a656ecf6](https://github.com/--arc/uuid-generator/commit/fb856f6486a1e76e8e3188be92593130a656ecf6))



